﻿using AsteroidWatch.Objects;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Security;
using System.Text;

namespace AsteroidWatch.DAL
{
    public class AsteroidDAO
    {
        public string ConnectionString { get; set; }
        private string SqlGetAllRecordedAsteroids = "SELECT * FROM asteroids;";
        private string SqlGetTop5ClosestAsteroids = "SELECT TOP(5) * FROM asteroids ORDER BY miss_distance";
        private string SqlUpdateRecords = "INSERT INTO asteroids(id, name, approach_date, miss_distance, diameter_max_miles," +
            " diameter_min_miles, diameter_max_feet, diameter_min_feet, relative_velocity, is_hazardous) VALUES(@ID, @Name," +
            " @ApproachDate, @MissDistance, @MaxMiles, @MinMiles, @MaxFeet, @MinFeet, @Velocity, @Hazardous); ";
        private string SqlGetClosestHazardousAsteroid = "SELECT TOP(1) * FROM asteroids WHERE is_hazardous = 1 ORDER BY miss_distance;";
        private string SqlDeleteOldAsteroidData = "DELETE FROM asteroids WHERE approach_date < DATEADD(day,-1,GETDATE());";

        private List<int> ExistingAsteroidIDs 
        {
            get
            {
                List<Asteroid> recordedRoids = GetAllRecordedAsteroids();
                if (recordedRoids == null) { return null; }
                List<int> result = new List<int>();
                foreach (Asteroid roid in recordedRoids)
                {
                    result.Add(roid.ID);
                }
                return result;
            }
        }

        public AsteroidDAO(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public List<Asteroid> GetTop5ClosestAsteroids()
        {
            return GetAsteroidsByQuery(SqlGetTop5ClosestAsteroids);
        }

        public List<Asteroid> GetAllRecordedAsteroids()
        {
            return GetAsteroidsByQuery(SqlGetAllRecordedAsteroids);
        }

        public Asteroid GetClosestHazardousAsteroid()
        {
            List<Asteroid> result = GetAsteroidsByQuery(SqlGetClosestHazardousAsteroid);
            if (result.Count > 0)
            {
                return result[0];
            }
            return null;
        }
        public List<Asteroid> GetAsteroidsByQuery(string sqlQuery)
        {
            List<Asteroid> existingAsteroids = new List<Asteroid>();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlQuery, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Asteroid roid = ParseSQLQueryToAsteroid(reader);
                        existingAsteroids.Add(roid);
                    }
                }
            }
            catch
            {
                return null;
            }
            return existingAsteroids;
        }
        
        public Asteroid ParseSQLQueryToAsteroid(SqlDataReader reader)
        {
            Asteroid roid = new Asteroid();
            roid.CloseApproachDate = ParseApproachDateStringToDateTime(Convert.ToString(reader["approach_date"]));
            roid.MissDistance = Convert.ToDouble(reader["miss_distance"]);
            roid.EstimatedDiameterMaxMiles = Convert.ToDouble(reader["diameter_max_miles"]);
            roid.EstimatedDiameterMinMiles = Convert.ToDouble(reader["diameter_min_miles"]);
            roid.EstimatedDiameterMaxFeet = Convert.ToDouble(reader["diameter_max_feet"]);
            roid.EstimatedDiameterMinFeet = Convert.ToDouble(reader["diameter_min_feet"]);
            roid.ID = Convert.ToInt32(reader["id"]);
            roid.Name = Convert.ToString(reader["name"]);
            roid.IsPotentiallyHazardous = Convert.ToBoolean(reader["is_hazardous"]);
            roid.RelativeVelocity = Convert.ToInt32(reader["relative_velocity"]);
            return roid;
        }

        public bool UpdateRecords(List<Asteroid> newData)
        {
            List<int> existingAsteroidIDs = ExistingAsteroidIDs;
            List<Asteroid> newAsteroids = new List<Asteroid>();
            if (existingAsteroidIDs != null)
            {
                newAsteroids = newData.Where(roid => { return !existingAsteroidIDs.Contains(roid.ID); }).ToList();
            }
            else { newAsteroids = newData; }
            this.DeleteOldAsteroidData();
            return AddNewAsteroidsToDatabase(newAsteroids) > 0;
        }

        public int AddNewAsteroidsToDatabase(List<Asteroid> newAsteroids)
        {
            int result = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    foreach (Asteroid roid in newAsteroids)
                    {
                        SqlCommand command = ParseRoidIntoCommand(connection, roid);
                        result = command.ExecuteNonQuery();
                    }
                }
            }
            catch { return 0; }
            return result;
        }

        public static DateTime ParseApproachDateStringToDateTime(string approachDate)
        {
            string[] dateParts = approachDate.Split("-");
            int year = int.Parse(dateParts[0]);
            int month = int.Parse(dateParts[1]);
            int day = int.Parse(dateParts[2]);
            return new DateTime(year, month, day);
        }

        private SqlCommand ParseRoidIntoCommand(SqlConnection connection, Asteroid roid)
        {
            SqlCommand command = new SqlCommand(SqlUpdateRecords, connection);
            command.Parameters.AddWithValue("@ID", roid.ID);
            command.Parameters.AddWithValue("@Name", roid.Name);
            command.Parameters.AddWithValue("@ApproachDate", UI.FormatDateTimeAsString(roid.CloseApproachDate));
            command.Parameters.AddWithValue("@MissDistance", roid.MissDistance);
            command.Parameters.AddWithValue("@MaxMiles", roid.EstimatedDiameterMaxMiles);
            command.Parameters.AddWithValue("@MinMiles", roid.EstimatedDiameterMinMiles);
            command.Parameters.AddWithValue("@MaxFeet", roid.EstimatedDiameterMaxFeet);
            command.Parameters.AddWithValue("@MinFeet", roid.EstimatedDiameterMinFeet);
            command.Parameters.AddWithValue("@Velocity", roid.RelativeVelocity);
            command.Parameters.AddWithValue("@Hazardous", roid.IsPotentiallyHazardous);
            return command;
        }

        private bool DeleteOldAsteroidData()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString)) 
            {
                connection.Open();
                SqlCommand command = new SqlCommand(SqlDeleteOldAsteroidData, connection);
                try
                { 
                command.ExecuteNonQuery();
                }
                catch { return false; }
            }
            return true;
        }
    }
}
