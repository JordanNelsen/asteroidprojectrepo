﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using AsteroidWatch.DAL;
using AsteroidWatch.Objects;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace AsteroidWatch
{
    public class API
    {
        public string BaseURL { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public RestClient Client { get; set; }

        public string MyAPIKey = "xEhuHbwdxJbg9lmmXRpdlQRddSXbzQcShNYl1mpb";
        public API()
        {
            this.StartDate = DateTime.Now;
            this.EndDate = StartDate.AddDays(7);
            this.BaseURL = "https://api.nasa.gov/neo/rest/v1/feed?start_date=XXX&end_date=ZZZ&api_key=DEMO_KEY";
            this.BaseURL = BaseURL.Replace("XXX", UI.FormatDateTimeAsString(StartDate));
            this.BaseURL = BaseURL.Replace("ZZZ", UI.FormatDateTimeAsString(EndDate));
            if (MyAPIKey != "") { this.BaseURL = BaseURL.Replace("DEMO_KEY", MyAPIKey); }
            Client = new RestClient(BaseURL);
        }

        public List<Asteroid> GetUpcomingData()
        {
            RestRequest request = new RestRequest(BaseURL);
            IRestResponse response = Client.Get(request);
            dynamic obj = JObject.Parse(response.Content);
            dynamic near_earth_objects = obj.near_earth_objects;

            List<Asteroid> asteroidList = new List<Asteroid>();

            foreach (dynamic dayOfData in near_earth_objects)
            {
                foreach (dynamic roidJSON in dayOfData.Value) {
                    Asteroid asteroid = ParseAsteriodFromJSON(roidJSON);
                    asteroidList.Add(asteroid);
                }
            }
            return asteroidList;
        }

        private Asteroid ParseAsteriodFromJSON(dynamic roid)
        {
            Asteroid result = new Asteroid();
            result.EstimatedDiameterMaxMiles = (double)roid["estimated_diameter"]["miles"]["estimated_diameter_max"];
            result.EstimatedDiameterMinMiles = (double)roid["estimated_diameter"]["miles"]["estimated_diameter_min"];
            result.EstimatedDiameterMaxFeet = (double)roid["estimated_diameter"]["feet"]["estimated_diameter_max"];
            result.EstimatedDiameterMinFeet = (double)roid["estimated_diameter"]["feet"]["estimated_diameter_min"];
            result.Name = roid["name"];
            result.ID = roid["id"];
            result.IsPotentiallyHazardous = roid["is_potentially_hazardous_asteroid"];
            result.CloseApproachDate = AsteroidDAO.ParseApproachDateStringToDateTime((string)roid["close_approach_data"][0]["close_approach_date"]);
            result.MissDistance = (double)roid["close_approach_data"][0]["miss_distance"]["miles"];
            result.RelativeVelocity = (double)roid["close_approach_data"][0]["relative_velocity"]["miles_per_hour"];
            return result;
        }
    }
}
