﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsteroidWatch.Objects
{
    public class Asteroid
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsPotentiallyHazardous { get; set; }
        public DateTime CloseApproachDate { get; set; }
        public double RelativeVelocity { get; set; }
        public double MissDistance { get; set; }
        public double EstimatedDiameterMinMiles { get; set; }
        public double EstimatedDiameterMaxMiles { get; set; }
        public double EstimatedDiameterMinFeet { get; set; }
        public double EstimatedDiameterMaxFeet { get; set; }
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append($"{Name}: ");
            result.Append("\n  " + (EstimatedDiameterMinMiles < .3 ? (Math.Round(EstimatedDiameterMaxFeet, 3).ToString() + " feet") : (Math.Round(EstimatedDiameterMaxMiles, 3).ToString() + " miles")));
            result.Append($" max diameter.\n  It will be {Math.Round(MissDistance, 3)} miles away on " + CloseApproachDate.ToShortDateString());
            result.Append($", traveling at {Math.Round(RelativeVelocity, 3)} mph.\n");
            result.Append(IsPotentiallyHazardous ? "  POTENTIALLY HAZARDOUS!\n" : "  Not a potential hazard.\n");
            return result.ToString();
        }
    }
}
