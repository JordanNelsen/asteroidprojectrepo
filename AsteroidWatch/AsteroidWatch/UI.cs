﻿using AsteroidWatch.DAL;
using AsteroidWatch.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace AsteroidWatch
{
    public class UI
    {
        private string ConnectionString = "Data Source=localhost\\sqlexpress;Initial Catalog=AsteroidWatch;Integrated Security=True";
        public AsteroidDAO Dao { get; set; }
        public API AsteroidAPI { get; set;}
        public UI()
        {
            this.Dao = new AsteroidDAO(ConnectionString);
            this.AsteroidAPI = new API();
        }
        public void RunUI()
        {
            while (true)
            {
                Console.WriteLine("Top 5 Closest Asteroids:\n");
                DisplayTop5ClosestUpcomingRoids();
                Asteroid closestHazardousRoid = Dao.GetClosestHazardousAsteroid();
                if (closestHazardousRoid != null) { Console.WriteLine("\nThe closest hazardous asteroid is: \n" + closestHazardousRoid); }

                Console.WriteLine("\nWould you like to update the asteroid database? (y/n)");
                string input = Console.ReadLine();
                switch (input.ToLower())
                {
                    case "y":
                        this.Dao.UpdateRecords(AsteroidAPI.GetUpcomingData());
                        break;
                    default:
                        Console.WriteLine("Then what do you want to do?");
                        Console.ReadLine();
                        break;
                }
            }
        }

        public static string FormatDateTimeAsString(DateTime date)
        {
            StringBuilder result = new StringBuilder(date.Year.ToString() + "-");
            if (date.Month < 10)
            {
                result.Append("0");
            }
            result.Append(date.Month.ToString() + "-");
            if (date.Day < 10)
            {
                result.Append("0");
            }
            result.Append(date.Day.ToString());
            return result.ToString();
        }

        public void DisplayTop5ClosestUpcomingRoids()
        {
            List<Asteroid> top5Closest = Dao.GetTop5ClosestAsteroids();
            foreach(Asteroid roid in top5Closest)
            {
                Console.WriteLine(roid);
            }
        }
    }
}
