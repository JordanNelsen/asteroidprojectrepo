﻿using System;
using System.Collections.Generic;
using AsteroidWatch.DAL;
using AsteroidWatch.Objects;
using RestSharp;

namespace AsteroidWatch
{
    class Program
    {
        static void Main(string[] args)
        {
            //string connectionString = "Data Source=localhost\\sqlexpress;Initial Catalog=AsteroidWatch;Integrated Security=True";
            //AsteroidDAO dao = new AsteroidDAO(connectionString);
            //API testAPI = new API();
            UI ui = new UI();
            ui.RunUI();
            Console.ReadLine();
        }
    }
}
