using Microsoft.VisualStudio.TestTools.UnitTesting;
using AsteroidWatch.DAL;
using AsteroidWatch.Objects;
using System;
using System.Transactions;
using System.Runtime.CompilerServices;
using AsteroidWatch;
using System.Collections.Generic;

namespace AsteroidWatchTests
{
    [TestClass]
    public class AsteroidDAOTests
    {
        [TestMethod]
        public void ParseApproachDateToDateTimeTest()
        {
            // Arrange
            string testString = "2021-2-28";
            DateTime expected = new DateTime(2021, 2, 28);
            // Act
            DateTime result = AsteroidDAO.ParseApproachDateStringToDateTime(testString);
            // Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void GetRecordedAsteroidsTest()
        {
            // Arrange
            UI testUI = new UI();
            // Act
            List<Asteroid> existingAsteroids = testUI.Dao.GetAllRecordedAsteroids();
            bool result = existingAsteroids.Count > 0;
            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void UpdateRecordsTest()
        {
            // Arrange
            UI testUI = new UI();
            TransactionScope transaction = new TransactionScope();
            Asteroid testRoid = new Asteroid();
            testRoid.ID = 1;
            testRoid.Name = "Gerald";
            testRoid.MissDistance = 1.1;
            testRoid.RelativeVelocity = 1.2;
            testRoid.IsPotentiallyHazardous = true;
            testRoid.CloseApproachDate = new DateTime(2021, 2, 2);
            testRoid.EstimatedDiameterMaxFeet = 3;
            testRoid.EstimatedDiameterMinFeet = 2;
            testRoid.EstimatedDiameterMaxMiles = .1;
            testRoid.EstimatedDiameterMinMiles = .01;
            List<Asteroid> testList = new List<Asteroid>();
            testList.Add(testRoid);
            try
            {
                // Act
                bool updateResult = testUI.Dao.UpdateRecords(testList);
                List<Asteroid> existingRoids = testUI.Dao.GetAllRecordedAsteroids();
                bool roidWasAdded = false;
                foreach (Asteroid roid in existingRoids)
                {
                    if (roid.ID == 1)
                    {
                        roidWasAdded = true;
                        break;
                    }
                }
                // Assert
                Assert.IsTrue(roidWasAdded);
                Assert.IsTrue(updateResult);
            }
            finally { transaction.Dispose(); }
        }
        [TestMethod]
        public void DeleteOldRecordsTest()
        {
            // Arrange
            UI testUI = new UI();
            TransactionScope transaction = new TransactionScope();
            Asteroid testRoid = new Asteroid();
            testRoid.ID = 1;
            testRoid.Name = "Gerald";
            testRoid.MissDistance = 1.1;
            testRoid.RelativeVelocity = 1.2;
            testRoid.IsPotentiallyHazardous = true;
            testRoid.CloseApproachDate = new DateTime(2001, 2, 2);
            testRoid.EstimatedDiameterMaxFeet = 3;
            testRoid.EstimatedDiameterMinFeet = 2;
            testRoid.EstimatedDiameterMaxMiles = .1;
            testRoid.EstimatedDiameterMinMiles = .01;
            List<Asteroid> testList = new List<Asteroid>();
            testList.Add(testRoid);
            try
            {
                // Act
                bool updateResult = testUI.Dao.UpdateRecords(testList);
                List<Asteroid> existingRoids = testUI.Dao.GetAllRecordedAsteroids();
                testUI.Dao.UpdateRecords(new List<Asteroid>());
                bool roidWasDeleted = true;
                foreach (Asteroid roid in existingRoids)
                {
                    if (roid.ID == 1)
                    {
                        roidWasDeleted = false;
                        break;
                    }
                }
                // Assert
                Assert.IsFalse(roidWasDeleted);
            }
            finally { transaction.Dispose(); }
        }
    }
}
